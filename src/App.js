import React from 'react';
import SelectMatrix from './components/SelectMatrix/SelectMatrix';
import './App.css';

function App (){
  return  (
    <div className="App">
      <div className="App-container">
        <SelectMatrix />
      </div>
    </div>
  )
}

export default App;