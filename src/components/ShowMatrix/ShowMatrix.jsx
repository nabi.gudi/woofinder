import React from '../../../node_modules/react';
import data from '../../resources/data';
import styles from './ShowMatrix.module.scss';

function renderMatrix(matrix) {
  let row = [];
  let column = [];
  for(let rowIndex = 0; rowIndex < matrix.length; rowIndex++) {
    for(let colIndex = 0; colIndex < matrix[rowIndex].length; colIndex++) {
      const item = matrix[rowIndex][colIndex];
     
      column.push(renderLetter(item));
    }
    row.push(renderRow(column));
    column = [];
  }
  return row;
}

function renderLetter(character){
  return <div className={`${styles.character}`}><label className={styles.label}>{character}</label></div>
}
function renderRow(row){
  return <div className={styles.row}>{row}</div>
}

function ShowMatrix ({matrix}) {
  return <div className={styles.matrix}>{renderMatrix(data.resources[matrix])}</div>
}

export default ShowMatrix;