import {useState} from 'react';
import React from '../../../node_modules/react';
import data from '../../resources/data';
import styles from './SolveMatrix.module.scss';

function getDiagonals(array) {
  var Ylength = array.length;
  var Xlength = array[0].length;
  var maxLength = Math.max(Xlength, Ylength);
  var temp;
  var returnArray = [];
  for (var k = 0; k <= 2 * (maxLength - 1); ++k) {
      temp = [];
      for (var y = Ylength - 1; y >= 0; --y) {
          var x = k - y;
          if (x >= 0 && x < Xlength) {
              temp.push(array[y][x]);
          }
      }
      if(temp.length > 0) {
          returnArray.push(temp.join(''));
      }
  }
  return returnArray;
}

function getArraysFromMatrix (matrix) {
  let horizontal = matrix.map((a) => a.join(''));
  let horizontalInverse = horizontal.map((a) => a.split("").reverse().join(""));

  let rotateMatrix = matrix.map((val, index) => matrix.map(row => row[index]).reverse());

  let vertical = rotateMatrix.map((a) => a.join(''));
  let verticalInverse = vertical.map((a) => a.split("").reverse().join(""));

  let diagonalA = getDiagonals(horizontal);
  let diagonalInverseA = getDiagonals(horizontal).map((a) => a.split("").reverse().join(""));

  let diagonalB = getDiagonals(vertical);
  let diagonalInverseB = getDiagonals(vertical).map((a) => a.split("").reverse().join(""));

  // console.log('horizontal:' + horizontal);
  // console.log('horizontal inversa:' + horizontalInverse);
  // console.log('vertical:' + vertical);
  // console.log('vertical inversa:' + verticalInverse);
  // console.log('diagonal izquierda-abajo derecha-arriba: '+  getDiagonals(horizontal) );
  // console.log('diagonal derecha-abajo izquierda-arriba: '+  getDiagonals(vertical) );
  // console.log('diagonal derecha-arriba izquierda-abajo : '+  diagonalInverseA );
  // console.log('diagonal izquierda-arriba derecha-abajo : '+  diagonalInverseB );

  return [...horizontal, ...horizontalInverse, ...vertical, ...verticalInverse, ...diagonalA, ...diagonalInverseA, ...diagonalB, ...diagonalInverseB]; 
}

function solveMatrix(matrix, word) {
  return matrix.reduce(function(p,c){
    return p = p + c.split(word).length - 1;
  },0);
}

function renderSolution(matrix, word){
  let reducedMatrix = getArraysFromMatrix(matrix);
  let solution = solveMatrix(reducedMatrix, word);
  const response = (
      solution ? 
      (
        <div className={styles.solution}>
          <label>La palabra </label>
          <strong>{word}</strong>
          <label> esta en la sopa de letras.</label>
          <label> Aparece </label>
          <strong>{solution}</strong>
          <label> veces.</label>
        </div>
      ): (

        <div className={styles.solution}>
        <label>La palabra no esta en la sopa.</label>
        </div>
      )
  )
  return response;
}

function SolveMatrix ({matrix, word}) {
  const [solution , setSolution] = useState(null);
  return (
    <div className={styles.ask}>
      <button className={styles.button} disabled={word===''} onClick={() => setSolution(renderSolution(data.resources[matrix], word))}>
        Buscar palabra
      </button>

      {/* {(word==='') ? setSolution(null) : null} */}
      {solution}
    </div>
  )
}

export default SolveMatrix;