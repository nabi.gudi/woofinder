import React , { useState } from 'react';
import styles from './SelectMatrix.module.scss';
import ShowMatrix from '../../components/ShowMatrix/ShowMatrix';
import SolveMatrix from '../../components/SolveMatrix/SolveMatrix';



function SelectMatrix () {
  let [matrix, setMatrix] = useState(0);
  let [word, setWord] = useState('');
  
  const changeMatrix = (value) => {
    setMatrix(value);
    setWord('');
  }

  return (
    <div className={styles.ask}>
      <h1 className={styles.title}>Sopa de Letras</h1>
      <h2 className={styles.subtitle}>¿Qué sopa de letras uso?</h2>


      <div class="select">
        <select id="standard-select" onChange={e => changeMatrix(e.currentTarget.value)}>
          <option value={0}>Matriz 1</option>
          <option value={1}>Matriz 2</option>
          <option value={2}>Matriz 3</option>
          <option value={3}>Matriz 4</option>
          <option value={4}>Matriz 5</option>
        </select>
      </div>

      <ShowMatrix matrix={matrix}/>

      <h2 className={styles.subtitle}>¿Qué palabra deseas buscar?</h2>

      <input className={styles.input} type="text" value={word} onChange={e => setWord(e.target.value)}/>
      
      <SolveMatrix matrix={matrix} word={word}/> 
    </div>
  )
}
  
  export default SelectMatrix;